# NintenTools.MarioKart8

The goal of this .NET library is to provide managed access to data stored in Mario Kart 8 (Deluxe) specific file formats. A collection of tools, based on the library, is offered aswell.

The library is available as a [NuGet package](https://www.nuget.org/packages/Syroot.NintenTools.MarioKart8).

Right now, the following formats and files are supported:

| Description             | File Name(s)     | Load   | Save | Notes                                                                |
|-------------------------|:----------------:|:------:|:----:|----------------------------------------------------------------------|
| BIN Files               | \*.bin           | Yes    | Yes  | Edit with BIN Editor. Parse with [binary template](https://gitlab.com/Syroot/NintenTools.MarioKart8/blob/master/other/010%20Binary%20Templates/MK8_BIN.bt). |
| Obj Definition Database | objflow.byaml    | Yes    | Yes  | Strongly typed via `Courses.Objs.ObjDefinitionDb`                    |
| Course Definitions      | \*_muunt\*.byaml | Yes    | Yes  | Strongly typed via `Courses.CourseDefinition`                        |
| Other BYAML Files       | \*.byaml         | Yes    | Yes  | Use [NintenTools.Byaml](https://gitlab.com/Syroot/NintenTools.Byaml) |
| Visual Models           | *.bfres          | Yes    | Yes  | Use [NintenTools.Bfres](https://gitlab.com/Syroot/NintenTools.Bfres) |
| KCL Collision Models    | course.kcl       | Yes    | Yes  | Experimental support. Parse with [binary template](https://gitlab.com/Syroot/NintenTools.MarioKart8/blob/master/other/010%20Binary%20Templates/MK8_KCL.bt) |
| Yaz0 Compressed Data    | \*.szs / others  | Yes    | No   | Use [NintenTools.Yaz0](https://gitlab.com/Syroot/NintenTools.Yazo)   |

## Tools

The repository contains the following tools. Please note these were developed for testing purposes and do not receive support.
- **BIN Editor**: Shows item probabilities of **Item.bin**, part physics of **Performance.bin** files, data in other BIN files and allows editing and saving new versions.
![BIN Editor](https://gitlab.com/Syroot/NintenTools.MarioKart8/raw/master/doc/readme/bin_editor.png)
- **Adjust200**: Takes a **course_muunt.byaml** and adds `Adjuster200cc` Objs into it from a **course_muunt_200.byaml** file in the same directory, overwriting the latter to create a new 200cc BYAML file.
- **BinDumper**: Dumps the data of **&ast;.bin** files as found in their section, group and element hierarchy.
- **NoLakitu**: Removes `EnemyPath` and `LapPath` from a **&ast;_muunt&ast;.byaml** (and Objs having referenced those) in order to get rid of Lakitu who would prevent you from going out of bounds.
- **ObjDumper**: Dumps the information found in **objflow.byaml** into a readable table.

## Deprecation Notice

**This project aswell as other NintenTools projects is no longer updated or maintained.**
The following known issues result from this:
- Several tools are dependent on _ancient_ and even unlisted NuGet package versions. [While those dependencies are not visible on its own on NuGet, they can still be restored](https://docs.microsoft.com/en-us/nuget/nuget-org/policies/deleting-packages). 
- BIN Editor may incorrectly present data from Mario Kart 8 Deluxe.
