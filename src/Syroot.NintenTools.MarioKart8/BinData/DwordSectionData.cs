﻿using Syroot.BinaryData;

namespace Syroot.NintenTools.MarioKart8.BinData
{
    /// <summary>
    /// Represents a 2-dimensional array of structs consisting of <see cref="Dword"/> instances.
    /// </summary>
    public class DwordSectionData : ISectionData
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private int _elementCount;

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the strongly-typed <see cref="Section"/> data.
        /// </summary>
        public Dword[][][] Data { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        /// <summary>
        /// Deserializes the section data available in the given <paramref name="reader"/> for the given
        /// <paramref name="section"/> with the specified <paramref name="sectionDataSize"/>.
        /// </summary>
        /// <param name="reader">The <see cref="BinaryDataReader"/> to deserialize data from.</param>
        /// <param name="section">The <see cref="Section"/> to read the data for.</param>
        /// <param name="sectionDataSize">The raw size of the section data without the header in bytes.</param>
        void ISectionData.Load(BinaryDataReader reader, Section section, int sectionDataSize)
        {
            _elementCount = sectionDataSize / section.ParamRepeat / section.ParamCount / sizeof(int);

            Data = new Dword[section.ParamRepeat][][];
            for (int i = 0; i < section.ParamRepeat; i++)
            {
                Data[i] = new Dword[section.ParamCount][];
                for (int j = 0; j < section.ParamCount; j++)
                {
                    Data[i][j] = new Dword[_elementCount];
                    for (int k = 0; k < _elementCount; k++)
                    {
                        Data[i][j][k] = reader.ReadInt32();
                    }
                }
            }
        }

        /// <summary>
        /// Serializes the section data into the given <paramref name="writer"/> for the given
        /// <paramref name="section"/>.
        /// </summary>
        /// <param name="writer">The <see cref="BinaryDataWriter"/> to serialize data with.</param>
        /// <param name="section">The <see cref="Section"/> to write the data for.</param>
        void ISectionData.Save(BinaryDataWriter writer, Section section)
        {
            section.ParamRepeat = (ushort)Data.Length;
            section.ParamCount = (ushort)Data[0].Length;

            for (int i = 0; i < section.ParamRepeat; i++)
            {
                for (int j = 0; j < section.ParamCount; j++)
                {
                    for (int k = 0; k < _elementCount; k++)
                    {
                        writer.Write(Data[i][j][k]);
                    }
                }
            }
        }
    }
}
