﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData.Dlc
{
    /// <summary>
    /// Represents the available section identifiers in a DLC.bin file.
    /// </summary>
    public enum SectionIdentifier : uint
    {
        DLCD = 0x444C4344,
        DLCB = 0x444C4342,
        DLCT = 0x444C4354,
        DLCW = 0x444C4357,
        DLCC = 0x444C4343
    }
}

#pragma warning restore 1591
