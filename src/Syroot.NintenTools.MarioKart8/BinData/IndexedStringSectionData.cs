﻿using System;
using System.Text;
using Syroot.BinaryData;

namespace Syroot.NintenTools.MarioKart8.BinData
{
    /// <summary>
    /// Represents a 2-dimensional array of indexed <see cref="String"/> instances.
    /// </summary>
    public class IndexedStringSectionData : ISectionData
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the strongly-typed <see cref="Section"/> data.
        /// </summary>
        public string[][] Data { get; private set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        /// <summary>
        /// Deserializes the section data available in the given <paramref name="reader"/> for the given
        /// <paramref name="section"/> with the specified <paramref name="sectionDataSize"/>.
        /// </summary>
        /// <param name="reader">The <see cref="BinaryDataReader"/> to deserialize data from.</param>
        /// <param name="section">The <see cref="Section"/> to read the data for.</param>
        /// <param name="sectionDataSize">The raw size of the section data without the header in bytes.</param>
        void ISectionData.Load(BinaryDataReader reader, Section section, int sectionDataSize)
        {
            Data = new string[section.ParamRepeat][];
            for (int i = 0; i < section.ParamRepeat; i++)
            {
                // Ignore the indices and read the offsets which are relative to the last offset.
                int[] offsets = new int[section.ParamCount];
                for (int j = 0; j < section.ParamCount; j++)
                {
                    reader.Position += sizeof(int); // First byte is index.
                    offsets[j] = reader.ReadInt32();
                }

                // Read the strings at the offsets.
                long offsetBase = reader.Position;
                string[] strings = new string[section.ParamCount];
                for (int j = 0; j < section.ParamCount; j++)
                {
                    reader.Position = offsetBase + offsets[j];
                    strings[j] = reader.ReadString(BinaryStringFormat.ZeroTerminated, Encoding.ASCII);
                }
                Data[i] = strings;
            }
        }

        /// <summary>
        /// Serializes the section data into the given <paramref name="writer"/> for the given
        /// <paramref name="section"/>.
        /// </summary>
        /// <param name="writer">The <see cref="BinaryDataWriter"/> to serialize data with.</param>
        /// <param name="section">The <see cref="Section"/> to write the data for.</param>
        void ISectionData.Save(BinaryDataWriter writer, Section section)
        {
            section.ParamRepeat = (ushort)Data.Length;
            section.ParamCount = (ushort)Data[0].Length;

            for (int i = 0; i < Data.Length; i++)
            {
                string[] strings = Data[i];

                // Write the indices and offsets.
                int offsetBase = 0;
                for (int j = 0; j < strings.Length; j++)
                {
                    writer.Write((byte)j);
                    writer.Align(4);
                    writer.Write(offsetBase);
                    offsetBase += strings[j].Length + 1;
                }

                // Write the strings.
                foreach (string str in strings)
                {
                    writer.Write(str, BinaryStringFormat.ZeroTerminated, Encoding.ASCII);
                }
            }
        }
    }
}
