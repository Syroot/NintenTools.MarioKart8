﻿#pragma warning disable 1591 // Single elements do not require to be documented.

namespace Syroot.NintenTools.MarioKart8.BinData.Audio
{
    /// <summary>
    /// Represents the available section identifiers in an Audio.bin file.
    /// </summary>
    public enum SectionIdentifier : uint
    {
        DRSE = 0x44525345,
        ENSE = 0x454E5345,
        VOSE = 0x564F5345,
        MASE = 0x4D415345,
        ALRG = 0x414C5247,
        ALRM = 0x414C524D,
        POSE = 0x504F5345,
        ASMI = 0x41534D49,
        RTMP = 0x52544D50,
        BGMC = 0x42474D43,
        CBGM = 0x4342474D,
        CDSE = 0x43445345,
        CISE = 0x43495345,
        DFXA = 0x44465841,
        DFXB = 0x44465842,
        DFXC = 0x44465843,
        CFXA = 0x43465841,
        CFXB = 0x43465842,
        CSUW = 0x43535557
    }
}

#pragma warning restore 1591
