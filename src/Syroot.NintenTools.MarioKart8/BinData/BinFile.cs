﻿using System;
using System.Collections.Generic;
using System.IO;
using Syroot.BinaryData;

namespace Syroot.NintenTools.MarioKart8.BinData
{
    /// <summary>
    /// Represents binary data organized in sections, groups, and items.
    /// </summary>
    public class BinFile : ILoadableFile, ISaveableFile
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _formatOffset = 12;
        private const int _format = 1000;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="BinFile"/> class.
        /// </summary>
        public BinFile()
        {
            Reset();
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BinFile"/> class from the file with the given
        /// <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to create the instance from.</param>
        public BinFile(string fileName)
        {
            Load(fileName);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="BinFile"/> class from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to create the instance from.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after creating the instance.
        /// </param>
        public BinFile(Stream stream, bool leaveOpen = false)
        {
            Load(stream, leaveOpen);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the unique file identifier.
        /// </summary>
        public uint ID { get; set; }

        /// <summary>
        /// Gets or sets the <see cref="BinFileFormat"/> into which the file is serialized.
        /// </summary>
        public BinFileFormat Format { get; set; } = BinFileFormat.MarioKart8;

        /// <summary>
        /// Gets the available <see cref="Section"/> instances in this file.
        /// </summary>
        public IList<Section> Sections { get; set; }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        /// <summary>
        /// Loads the instance data from the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to load the instance data from.</param>
        public void Load(string fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.Read))
            {
                Load(fileStream);
            }
        }

        /// <summary>
        /// Loads the instance data from the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to load the instance data from.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after loading the instance.
        /// </param>
        public void Load(Stream stream, bool leaveOpen = false)
        {
            Reset();
            using (BinaryDataReader reader = new BinaryDataReader(stream, leaveOpen))
            {
                // Detect the format and byte order of this file.
                using (reader.TemporarySeek(_formatOffset))
                {
                    Format = reader.ReadEnum<BinFileFormat>(true);
                }
                if (Format == BinFileFormat.MarioKart8)
                {
                    reader.ByteOrder = ByteOrder.BigEndian;
                }

                // Read the header.
                ID = reader.ReadUInt32();
                uint fileSize = reader.ReadUInt32(); // Slightly off at times.
                ushort sectionCount = reader.ReadUInt16();
                ushort headerSize = reader.ReadUInt16();
                uint format = reader.ReadUInt32();
                int[] sectionOffsets = reader.ReadInt32s(sectionCount);

                // Read the sections.
                Sections = new List<Section>(sectionCount);
                for (int i = 0; i < sectionCount; i++)
                {
                    int sectionOffset = sectionOffsets[i];
                    int sectionStart = headerSize + sectionOffset;
                    int sectionEnd = i == sectionCount - 1 ? (int)reader.Length : headerSize + sectionOffsets[i + 1];
                    int sectionSize = sectionEnd - sectionStart;

                    reader.Position = sectionStart;
                    Section section = new Section()
                    {
                        ID = reader.ReadUInt32(),
                        ParamCount = reader.ReadUInt16(),
                        ParamRepeat = reader.ReadUInt16(),
                        ParamDeluxe = Format == BinFileFormat.MarioKart8Deluxe ? reader.ReadUInt32() : default(uint),
                        ParamUnknown = reader.ReadUInt32()
                    };
                    int sectionDataSize = sectionSize - ((int)reader.Position - sectionStart);
                    section.Data = (ISectionData)Activator.CreateInstance(GetSectionDataType(section), true);
                    section.Data.Load(reader, section, sectionDataSize);
                    Sections.Add(section);
                }
            }
        }

        /// <summary>
        /// Saves the instance data in the file with the given <paramref name="fileName"/>.
        /// </summary>
        /// <param name="fileName">The name of the file to save the instance data in.</param>
        public void Save(string fileName)
        {
            using (FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.Read))
            {
                Save(fileStream);
            }
        }

        /// <summary>
        /// Saves the instance data in the given <paramref name="stream"/>.
        /// </summary>
        /// <param name="stream">The <see cref="Stream"/> to save the instance data in.</param>
        /// <param name="leaveOpen"><c>true</c> to leave <paramref name="stream"/> open after saving the instance.
        /// </param>
        public void Save(Stream stream, bool leaveOpen = false)
        {
            using (BinaryDataWriter writer = new BinaryDataWriter(stream, leaveOpen))
            {
                // Set the byte order of this file.
                writer.ByteOrder = Format == BinFileFormat.MarioKart8 ? ByteOrder.BigEndian : ByteOrder.LittleEndian;

                // Write the header.
                writer.Write(ID);
                Offset fileSizeOffset = writer.ReserveOffset();
                writer.Write((ushort)Sections.Count);
                int headerSize = 16 + sizeof(int) * Sections.Count;
                writer.Write((ushort)headerSize);
                writer.Write(_format);
                Offset[] sectionOffsets = writer.ReserveOffset(Sections.Count);

                // Write the sectons.
                for (int i = 0; i < Sections.Count; i++)
                {
                    Section section = Sections[i];
                    sectionOffsets[i].Satisfy((int)(writer.Position - headerSize));

                    writer.Write(section.ID);
                    writer.Write(section.ParamCount);
                    writer.Write(section.ParamRepeat);
                    if (Format == BinFileFormat.MarioKart8Deluxe) writer.Write(section.ParamDeluxe);
                    writer.Write(section.ParamUnknown);
                    section.Data.Save(writer, section);
                    writer.Align(sizeof(int));
                }

                // Ensure alignment is actually written and store file size.
                writer.BaseStream.SetLength(writer.Position);
                fileSizeOffset.Satisfy();
            }
        }

        /// <summary>
        /// Returns the <see cref="Section"/> with the given <paramref name="id"/>. If no such section exists, an
        /// <see cref="ArgumentException"/> is thrown.
        /// </summary>
        /// <param name="id">The ID of the <see cref="Section"/> to retrieve.</param>
        /// <returns>The <see cref="Section"/> with the given ID.</returns>
        /// <exception cref="ArgumentException">No section with the given ID exists.</exception>
        public Section GetSectionByID(uint id)
        {
            foreach (Section section in Sections)
            {
                if (section.ID == id)
                    return section;
            }
            throw new ArgumentException($"Could not find section with ID {id}.", nameof(id));
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        private void Reset()
        {
            ID = default(uint);
            Format = default(BinFileFormat);
            Sections = null;
        }

        private Type GetSectionDataType(Section section)
        {
            switch (section.ID)
            {
                case (uint)Item.SectionIdentifier.RaceTitleSets:
                case (uint)Item.SectionIdentifier.RaceSets:
                case (uint)Item.SectionIdentifier.RaceDistances:
                case (uint)Item.SectionIdentifier.BattleTitleSets:
                case (uint)Item.SectionIdentifier.BattleSets:
                case (uint)Item.SectionIdentifier.BattleDistances:
                case (uint)Item.SectionIdentifier.ITDR:
                case (uint)Item.SectionIdentifier.ITRG:
                case (uint)Item.SectionIdentifier.ITGF:
                case (uint)Item.SectionIdentifier.ITDP:
                case (uint)Performance.SectionIdentifier.WeightStats:
                case (uint)Performance.SectionIdentifier.AccelerationStats:
                case (uint)Performance.SectionIdentifier.OnroadStats:
                case (uint)Performance.SectionIdentifier.OffroadStats:
                case (uint)Performance.SectionIdentifier.TurboStats:
                case (uint)Performance.SectionIdentifier.SpeedGroundStats:
                case (uint)Performance.SectionIdentifier.SpeedWaterStats:
                case (uint)Performance.SectionIdentifier.SpeedAntigravityStats:
                case (uint)Performance.SectionIdentifier.SpeedAirStats:
                case (uint)Performance.SectionIdentifier.HandlingGroundStats:
                case (uint)Performance.SectionIdentifier.HandlingWaterStats:
                case (uint)Performance.SectionIdentifier.HandlingAntigravityStats:
                case (uint)Performance.SectionIdentifier.HandlingAirStats:
                case (uint)Performance.SectionIdentifier.KartPoints:
                case (uint)Performance.SectionIdentifier.DriverPoints:
                case (uint)Performance.SectionIdentifier.TirePoints:
                case (uint)Performance.SectionIdentifier.GliderPoints:
                    return typeof(DwordSectionData);
                case (uint)MiiVoice.SectionIdentifier.MIVF:
                    return typeof(IndexedStringSectionData);
                case (uint)Audio.SectionIdentifier.DRSE:
                    return typeof(StringSectionData);
                // Experimental
                case (uint)AIBattle.SectionIdentifier.ABLB:
                case (uint)AIBattle.SectionIdentifier.ABKD:
                case (uint)AIBattle.SectionIdentifier.ABBB:
                case (uint)AIBattle.SectionIdentifier.ABCN:
                case (uint)AIBattle.SectionIdentifier.ABSN:
                case (uint)AIBattle.SectionIdentifier.ABPA:
                case (uint)AIRace.SectionIdentifier.ARAP:
                case (uint)AIRace.SectionIdentifier.ARCS:
                case (uint)AIRace.SectionIdentifier.ARLV:
                case (uint)AIRace.SectionIdentifier.ARIP:
                case (uint)AIRace.SectionIdentifier.ARCF:
                case (uint)AIRace.SectionIdentifier.ARPM:
                case (uint)AIRace.SectionIdentifier.ARIV:
                case (uint)Audio.SectionIdentifier.VOSE:
                case (uint)Audio.SectionIdentifier.ALRM:
                case (uint)Audio.SectionIdentifier.RTMP:
                case (uint)Audio.SectionIdentifier.BGMC:
                case (uint)Audio.SectionIdentifier.DFXA:
                case (uint)Audio.SectionIdentifier.DFXB:
                case (uint)Audio.SectionIdentifier.DFXC:
                case (uint)Audio.SectionIdentifier.CFXA:
                case (uint)Audio.SectionIdentifier.CFXB:
                case (uint)Course.SectionIdentifier.CSPM:
                case (uint)Course.SectionIdentifier.CSTP:
                case (uint)Dlc.SectionIdentifier.DLCD:
                case (uint)Dlc.SectionIdentifier.DLCB:
                case (uint)Dlc.SectionIdentifier.DLCT:
                case (uint)Dlc.SectionIdentifier.DLCW:
                case (uint)Dlc.SectionIdentifier.DLCC:
                case (uint)Highlight.SectionIdentifier.ID_17A9:
                case (uint)Highlight.SectionIdentifier.ID_270E:
                case (uint)MiiVoice.SectionIdentifier.MIVI:
                case (uint)MiiVoice.SectionIdentifier.MNFP:
                case (uint)Obj.SectionIdentifier.OSSP:
                case (uint)Obj.SectionIdentifier.OCMY:
                case (uint)Obj.SectionIdentifier.SLPM:
                case (uint)Obj.SectionIdentifier.CRCR:
                case (uint)Obj.SectionIdentifier.JPPK:
                case (uint)Obj.SectionIdentifier.VOLP:
                case (uint)Obj.SectionIdentifier.CAUS:
                case (uint)OpenFlag.SectionIdentifier.OFPT:
                case (uint)OpenFlag.SectionIdentifier.OFDF:
                case (uint)OpenFlag.SectionIdentifier.OFBF:
                case (uint)OpenFlag.SectionIdentifier.OFTF:
                case (uint)OpenFlag.SectionIdentifier.OFWF:
                case (uint)Parts.SectionIdentifier.DTOM:
                case (uint)Parts.SectionIdentifier.BODY:
                case (uint)Parts.SectionIdentifier.TIRE:
                case (uint)Parts.SectionIdentifier.WING:
                case (uint)Parts.SectionIdentifier.BDDR:
                case (uint)Parts.SectionIdentifier.WDDR:
                case (uint)Parts.SectionIdentifier.BDTR:
                case (uint)Parts.SectionIdentifier.BDWG:
                case (uint)Parts.SectionIdentifier.TRBD:
                case (uint)Parts.SectionIdentifier.TIRP:
                case (uint)Parts.SectionIdentifier.DREF:
                case (uint)Parts.SectionIdentifier.TIEF:
                case (uint)Parts.SectionIdentifier.BDEF:
                case (uint)UI.SectionIdentifier.UIMM:
                    return typeof(DwordSectionData);
                default:
                    return typeof(RawSectionData);
            }
        }
    }

    /// <summary>
    /// Represents the known formats of <see cref="BinFile"/> data.
    /// </summary>
    public enum BinFileFormat : uint
    {
        /// <summary>
        /// The original Mario Kart 8 format, stored in big endian.
        /// </summary>
        MarioKart8 = 0xE8030000, // 1000 BE

        /// <summary>
        /// The slightly extended Mario Kart 8 Deluxe format, stored in little endian.
        /// </summary>
        MarioKart8Deluxe = 0x000003E8 // 1000 LE
    }
}
