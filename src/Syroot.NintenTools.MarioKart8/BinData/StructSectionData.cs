﻿using Syroot.BinaryData;

namespace Syroot.NintenTools.MarioKart8.BinData
{
    /// <summary>
    /// Represents a 2-dimensional array of instances of a specific struct.
    /// </summary>
    public class StructSectionData<T> : ISectionData
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the strongly-typed <see cref="Section"/> data.
        /// </summary>
        public T[][] Data { get; set; }

        // ---- METHODS ------------------------------------------------------------------------------------------------

        /// <summary>
        /// Deserializes the section data available in the given <paramref name="reader"/> for the given
        /// <paramref name="section"/> with the specified <paramref name="sectionDataSize"/>.
        /// </summary>
        /// <param name="reader">The <see cref="BinaryDataReader"/> to deserialize data from.</param>
        /// <param name="section">The <see cref="Section"/> to read the data for.</param>
        /// <param name="sectionDataSize">The raw size of the section data without the header in bytes.</param>
        void ISectionData.Load(BinaryDataReader reader, Section section, int sectionDataSize)
        {
            Data = new T[section.ParamRepeat][];
            for (int i = 0; i < section.ParamRepeat; i++)
            {
                Data[i] = reader.ReadObjects<T>(section.ParamCount);
            }
        }

        /// <summary>
        /// Serializes the section data into the given <paramref name="writer"/> for the given
        /// <paramref name="section"/>.
        /// </summary>
        /// <param name="writer">The <see cref="BinaryDataWriter"/> to serialize data with.</param>
        /// <param name="section">The <see cref="Section"/> to write the data for.</param>
        void ISectionData.Save(BinaryDataWriter writer, Section section)
        {
            section.ParamRepeat = (ushort)Data.Length;
            section.ParamCount = (ushort)Data[0].Length;

            for (int i = 0; i < section.ParamRepeat; i++)
            {
                writer.WriteObject(Data[i]);
            }
        }
    }
}
