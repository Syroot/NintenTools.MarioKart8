using System.Reflection;
using System.Runtime.InteropServices;

[assembly: AssemblyTitle("MK8 BIN Editor")]
[assembly: AssemblyDescription("MK8 BIN Editor")]
[assembly: AssemblyCompany("(c) Syroot")]
[assembly: AssemblyProduct("MK8 BIN Editor")]
[assembly: AssemblyCopyright("MIT")]
[assembly: AssemblyVersion("2.1.1")]
[assembly: AssemblyFileVersion("2.1.1")]
[assembly: ComVisible(false)]
[assembly: Guid("3ca5df1a-f3fc-4528-9683-bfb4de671b53")]

namespace Syroot.NintenTools.MarioKart8.BinEditor
{
    internal static class AssemblyInfo
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        internal static string Title
        {
            get { return Assembly.GetEntryAssembly().GetCustomAttribute<AssemblyTitleAttribute>().Title; }
        }
    }
}
