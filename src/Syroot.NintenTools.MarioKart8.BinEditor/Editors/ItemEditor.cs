using System.Drawing;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinData.Item;
using Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Generic;
using Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Item;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.Editors
{
    /// <summary>
    /// Represents the main window of the application.
    /// </summary>
    internal partial class ItemEditor : EditorBase
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const string _allItems = "All Items";
        private const string _mushroomsOnly = "Mushrooms Only";
        private const string _shellsOnly = "Shells Only";
        private const string _bananasOnly = "Bananas Only";
        private const string _bobombsOnly = "Bob-ombs Only";
        private const string _franticMode = "Frantic Mode";
        private const string _grandPrix = "Grand Prix";
        private const string _shellsBananasOnly = "Shells && Bananas Only";
        private const string _distances = "Distances";
        private const string _humanPlayer = "Human Player";
        private const string _aiPlayer = "AI Player";

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the accentual color used for category rows in the user interface.
        /// </summary>
        internal override Color AccentColor
        {
            get { return Color.FromArgb(200, 100, 0); }
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Returns whether the editor can edit the given BIN file.
        /// </summary>
        internal override bool CheckBinFile(BinFile binFile)
        {
            return binFile.ID == (uint)FileIdentifier.Item;
        }

        /// <summary>
        /// Gets the <see cref="RowTreeNode"/> of the first hierarchy level to show for navigation.
        /// </summary>
        /// <returns>The <see cref="RowTreeNode"/> to display.</returns>
        internal override RowTreeNode GetCategoryTree()
        {
            RowTreeNode node = new RowTreeNode(null, new[]
            {
                CreateNode("Versus Races", new[]
                {
                    CreateNode(_allItems, new[]
                    {
                        CreateNode(_humanPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.All, false)),
                        CreateNode(_aiPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.AllAI, true))
                    }),
                    CreateNode(_shellsOnly, new[]
                    {
                        CreateNode(_humanPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.Shells, false)),
                        CreateNode(_aiPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.ShellsAI, true))
                    }),
                    CreateNode(_bananasOnly, new[]
                    {
                        CreateNode(_humanPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.Bananas, false)),
                        CreateNode(_aiPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.BananasAI, true))
                    }),
                    CreateNode(_mushroomsOnly, new[]
                    {
                        CreateNode(_humanPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.Mushrooms, false)),
                        CreateNode(_aiPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.MushroomsAI, true))
                    }),
                    CreateNode(_bobombsOnly, new[]
                    {
                        CreateNode(_humanPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.Bobombs, false)),
                        CreateNode(_aiPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.BobombsAI, true))
                    }),
                    CreateNode(_franticMode, new[]
                    {
                        CreateNode(_humanPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.Frantic, false)),
                        CreateNode(_aiPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.FranticAI, true))
                    }),
                    CreateNode(_grandPrix, new[]
                    {
                        CreateNode(_humanPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.GrandPrix, false)),
                        CreateNode(_aiPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.GrandPrixAI, true))
                    }),
                    CreateNode(_distances, new RaceDistanceDataAdapter())
                }),
                CreateNode("Battle Mode", new[]
                {
                    CreateNode(_allItems, new[]
                    {
                        CreateNode(_humanPlayer, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.All, false)),
                        CreateNode(_aiPlayer, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.AllAI, true))
                    }),
                    CreateNode(_shellsOnly, new[]
                    {
                        CreateNode(_humanPlayer, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.Shells, false)),
                        CreateNode(_aiPlayer, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.ShellsAI, true))
                    }),
                    CreateNode(_bananasOnly, new[]
                    {
                        CreateNode(_humanPlayer, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.Bananas, false)),
                        CreateNode(_aiPlayer, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.BananasAI, true))
                    }),
                    CreateNode(_mushroomsOnly, new[]
                    {
                        CreateNode(_humanPlayer, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.Mushrooms, false)),
                        CreateNode(_aiPlayer, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.MushroomsAI, true))
                    }),
                    CreateNode(_bobombsOnly, new[]
                    {
                        CreateNode(_humanPlayer, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.Bobombs, false)),
                        CreateNode(_aiPlayer, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.BobombsAI, true))
                    }),
                    CreateNode(_franticMode, new[]
                    {
                        CreateNode(_humanPlayer, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.Frantic, false)),
                        CreateNode(_aiPlayer, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.FranticAI, true))
                    }),
                }),
                CreateNode("Unused Versus Races", new[]
                {
                    CreateNode(_mushroomsOnly + " 1", new[]
                    {
                        CreateNode(_humanPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.UnusedMushrooms1, false)),
                        CreateNode(_aiPlayer, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.UnusedMushrooms1AI, true))
                    }),
                    CreateNode(_mushroomsOnly + " 2", new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.UnusedMushrooms2, false)),
                    CreateNode(_shellsOnly, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.UnusedShells, false)),
                    CreateNode(_bananasOnly, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.UnusedBananas, false)),
                    CreateNode(_bobombsOnly, new RaceDataAdapter(SectionIdentifier.RaceSets, RaceItemSet.Bobombs, false))
                }),
                CreateNode("Unused Battle Mode", new[]
                {
                    CreateNode(_shellsBananasOnly + " 1", new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.UnusedShellsBananas1, false)),
                    CreateNode(_shellsBananasOnly + " 2", new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.UnusedShellsBananas2, false)),
                    CreateNode(_mushroomsOnly, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.UnusedMushrooms, false)),
                    CreateNode(_shellsOnly, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.UnusedShells, false)),
                    CreateNode(_bananasOnly, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.UnusedBananas, false)),
                    CreateNode(_bobombsOnly, new BattleDataAdapter(SectionIdentifier.BattleSets, BattleItemSet.UnusedBobombs, false)),
                }),
                CreateNode("Title Versus Races", new[]
                {
                    CreateNode(_grandPrix, new[]
                    {
                        CreateNode(_humanPlayer, new RaceDataAdapter(SectionIdentifier.RaceTitleSets, RaceItemSet.GrandPrix, false)),
                        CreateNode(_aiPlayer, new RaceDataAdapter(SectionIdentifier.RaceTitleSets, RaceItemSet.GrandPrixAI, true))
                    }),
                    CreateNode(_allItems, new[]
                    {
                        CreateNode(_humanPlayer, new RaceDataAdapter(SectionIdentifier.RaceTitleSets, RaceItemSet.All, false)),
                        CreateNode(_aiPlayer, new RaceDataAdapter(SectionIdentifier.RaceTitleSets, RaceItemSet.AllAI, true))
                    }),
                }),
                CreateNode("Title Battle Mode", new BattleDataAdapter(SectionIdentifier.BattleTitleSets, BattleItemSet.All, false))
            });

            // TODO: Add additional Deluxe sections.
            if (Program.App.BinFile.Format == BinFileFormat.MarioKart8Deluxe)
            {
                node.Children.Add(new RowTreeNode("Deluxe Sections", new[]
                {
                    CreateSectionNode(SectionIdentifier.ITDR, true),
                    CreateSectionNode(SectionIdentifier.ITRG, true),
                    CreateSectionNode(SectionIdentifier.ITGF, false),
                    CreateSectionNode(SectionIdentifier.ITDP, true)
                }));
            }

            return node;
        }

        private RowTreeNode CreateSectionNode(SectionIdentifier id, bool transposed)
        {
            Section section = Program.App.BinFile.GetSectionByID((uint)id);
            DwordSectionData sectionData = (DwordSectionData)section.Data;

            RowTreeNode node = CreateNode(section.Name);
            if (sectionData.Data.Length == 1)
            {
                // The section has only one table, display it immediately.
                node.Tag = new GenericDataAdapter(sectionData.Data[0], transposed);
            }
            else
            {
                // The section has multiple tables, create a subnode for each.
                for (int i = 0; i < sectionData.Data.Length; i++)
                {
                    RowTreeNode childNode = new RowTreeNode((i + 1).ToString());
                    childNode.Tag = new GenericDataAdapter(sectionData.Data[i], transposed);
                    node.Children.Add(childNode);
                }
            }
            return node;
        }
    }
}
