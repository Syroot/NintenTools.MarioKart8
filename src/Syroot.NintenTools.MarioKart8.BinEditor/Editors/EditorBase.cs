using System.Drawing;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.Editors
{
    /// <summary>
    /// Represents the common interface and behavior for an editor handling a specific BIN format.
    /// </summary>
    internal abstract class EditorBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets the accentual color used for category rows in the user interface.
        /// </summary>
        internal abstract Color AccentColor { get; }
        
        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Returns whether the editor can edit the given BIN file.
        /// </summary>
        /// <param name="binFile">The <see cref="BinFile"/> to check.</param>
        internal abstract bool CheckBinFile(BinFile binFile);

        /// <summary>
        /// Gets the <see cref="RowTreeNode"/> of the first hierarchy level to show for navigation.
        /// </summary>
        /// <returns>The <see cref="RowTreeNode"/> to display.</returns>
        internal abstract RowTreeNode GetCategoryTree();
        
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Creates a <see cref="RowTreeNode"/> with the given <paramref name="text"/> and <paramref name="children"/>.
        /// </summary>
        /// <param name="text">The text to display.</param>
        /// <param name="children">The children of the node.</param>
        /// <returns>The new <see cref="RowTreeNode"/> instance.</returns>
        protected static RowTreeNode CreateNode(string text, params RowTreeNode[] children)
        {
            return new RowTreeNode(text, children);
        }

        /// <summary>
        /// Creates a <see cref="RowTreeNode"/> with the given <paramref name="text"/>, <paramref name="dataAdapter"/>
        /// and <paramref name="children"/>.
        /// </summary>
        /// <param name="text">The text to display.</param>
        /// <param name="dataAdapter">The <see cref="DataAdapter"/> to use for this node.</param>
        /// <param name="children">The children of the node.</param>
        /// <returns>The new <see cref="RowTreeNode"/> instance.</returns>
        protected static RowTreeNode CreateNode(string text, DataAdapter dataAdapter, params RowTreeNode[] children)
        {
            return new RowTreeNode(text, children) { Tag = dataAdapter };
        }
    }
}
