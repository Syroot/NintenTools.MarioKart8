using System.Drawing;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Performance;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.Editors
{
    /// <summary>
    /// Represents the main window of the application.
    /// </summary>
    internal partial class PerformanceEditor : EditorBase
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly RowTreeNode _categoryTree = new RowTreeNode(null, new[]
        {
            CreateNode("Points", new[]
            {
                CreateNode("Drivers", new PointsDriversDataAdapter()),
                CreateNode("Karts", new PointsKartsDataAdapter()),
                CreateNode("Tires", new PointsTiresDataAdapter()),
                CreateNode("Gliders", new PointsGlidersDataAdapter())
            }),
            CreateNode("Physics", new[]
            {
                CreateNode("Weight", new PhysicsWeightDataAdapter()),
                CreateNode("Acceleration", new PhysicsAccelerationDataAdapter()),
                CreateNode("On-Road Slip", new PhysicsOnroadDataAdapter()),
                CreateNode("Off-Road Slip", new PhysicsOffroadSlipDataAdapter()),
                CreateNode("Off-Road Brake", new PhysicsOffroadBrakeDataAdapter()),
                CreateNode("Turbo", new PhysicsTurboDataAdapter())
            }),
            CreateNode("Speed", new[]
            {
                CreateNode("Ground", new SpeedGroundDataAdapter()),
                CreateNode("Water", new SpeedWaterDataAdapter()),
                CreateNode("Anti-Gravity", new SpeedAntigravityDataAdapter()),
                CreateNode("Gliding", new SpeedGlidingDataAdapter())
            }),
            CreateNode("Handling", new[]
            {
                CreateNode("Ground", new HandlingGroundDataAdapter()),
                CreateNode("Water", new HandlingWaterDataAdapter()),
                CreateNode("Anti-Gravity", new HandlingAntigravityDataAdapter()),
                CreateNode("Gliding", new HandlingGlidingDataAdapter())
            })
        });
        
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets the accentual color used for category rows in the user interface.
        /// </summary>
        internal override Color AccentColor
        {
            get { return Color.FromArgb(0, 66, 200); }
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Returns whether the editor can edit the given BIN file.
        /// </summary>
        /// <param name="binFile">The <see cref="BinFile"/> to check.</param>
        internal override bool CheckBinFile(BinFile binFile)
        {
            return binFile.ID == (uint)FileIdentifier.Performance;
        }

        /// <summary>
        /// Gets the <see cref="RowTreeNode"/> of the first hierarchy level to show for navigation.
        /// </summary>
        /// <returns>The <see cref="RowTreeNode"/> to display.</returns>
        internal override RowTreeNode GetCategoryTree()
        {
            return _categoryTree;
        }
    }
}
