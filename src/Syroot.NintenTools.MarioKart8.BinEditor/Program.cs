using System;
using System.Reflection;
using System.Windows.Forms;
using Syroot.NintenTools.MarioKart8.BinEditor.Editors;

namespace Syroot.NintenTools.MarioKart8.BinEditor
{
    /// <summary>
    /// Represents the main class of the application containing the program entry point.
    /// </summary>
    internal static class Program
    {
        // ---- EVENTS -------------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the <see cref="ResourceLoader"/> which can be used to access resources in this assembly.
        /// </summary>
        internal static ResourceLoader R { get; private set; }

        /// <summary>
        /// Gets the <see cref="BinEditor.App"/> representing this application.
        /// </summary>
        internal static App App { get; private set; }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------

        [STAThread]
        private static void Main(string[] args)
        {
            // Create a resource loader.
            R = new ResourceLoader(Assembly.GetEntryAssembly(), "Syroot.NintenTools.MarioKart8.BinEditor.Resources");

            // Create the application.
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            using (App = new App(new ItemEditor(), new PerformanceEditor(), new GenericEditor()))
            {
                Application.Run(App);
            }
        }
    }
}
