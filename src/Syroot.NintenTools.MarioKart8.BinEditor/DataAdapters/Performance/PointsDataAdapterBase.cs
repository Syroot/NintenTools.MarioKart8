using System;
using System.Collections.Generic;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Performance
{
    internal abstract class PointsDataAdapterBase : DataAdapter
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override IEnumerable<TableHeader> Columns
        {
            get
            {
                yield return new TableHeader("Weight");
                yield return new TableHeader("Acceleration");
                yield return new TableHeader($"On-Road{Environment.NewLine}Traction");
                yield return new TableHeader($"Off-Road{Environment.NewLine}Traction");
                yield return new TableHeader("Turbo");
                yield return new TableHeader($"Speed{Environment.NewLine}Ground");
                yield return new TableHeader($"Speed{Environment.NewLine}Water");
                yield return new TableHeader($"Speed{Environment.NewLine}Anti-Gravity");
                yield return new TableHeader($"Speed{Environment.NewLine}Gliding");
                yield return new TableHeader($"Handling{Environment.NewLine}Ground");
                yield return new TableHeader($"Handling{Environment.NewLine}Water");
                yield return new TableHeader($"Handling{Environment.NewLine}Anti-Gravity");
                yield return new TableHeader($"Handling{Environment.NewLine}Gliding");
            }
        }
    }
}
