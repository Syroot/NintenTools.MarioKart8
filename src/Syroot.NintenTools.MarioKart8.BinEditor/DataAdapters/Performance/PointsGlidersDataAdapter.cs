using System.Collections.Generic;
using System.Linq;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinData.Performance;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Performance
{
    internal class PointsGlidersDataAdapter : PointsDataAdapterBase
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly List<TableHeader> _headers = new List<TableHeader>()
        {
            new TableHeader("Super Glider", Program.R.GetBitmap("Gliders.SuperGlider.png")),
            new TableHeader("Cloud Glider", Program.R.GetBitmap("Gliders.CloudGlider.png")),
            new TableHeader("Wario Wing", Program.R.GetBitmap("Gliders.WarioWing.png")),
            new TableHeader("Waddle Wing", Program.R.GetBitmap("Gliders.WaddleWing.png")),
            new TableHeader("Peach Parasol", Program.R.GetBitmap("Gliders.PeachParasol.png")),
            new TableHeader("Parachute", Program.R.GetBitmap("Gliders.Parachute.png")),
            new TableHeader("Parafoil", Program.R.GetBitmap("Gliders.Parafoil.png")),
            new TableHeader("Flower Glider", Program.R.GetBitmap("Gliders.FlowerGlider.png")),
            new TableHeader("Bowser Kite", Program.R.GetBitmap("Gliders.BowserKite.png")),
            new TableHeader("Plane Glider", Program.R.GetBitmap("Gliders.PlaneGlider.png")),
            new TableHeader("MKTV Parafoil", Program.R.GetBitmap("Gliders.MktvParafoil.png")),
            new TableHeader("Gold Glider", Program.R.GetBitmap("Gliders.GoldGlider.png")),
            new TableHeader("Hylian Kite", Program.R.GetBitmap("Gliders.HylianKite.png")),
            new TableHeader("Paper Glider", Program.R.GetBitmap("Gliders.PaperGlider.png"))
        };

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.GliderPoints);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }

        protected internal override IEnumerable<TableHeader> Rows
        {
            get { return _headers.Take(Array.GetLength(0)); }
        }

        protected internal override string RowHeaderTitle
        {
            get { return "Glider"; }
        }
    }
}
