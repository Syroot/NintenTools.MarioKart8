using System.Collections.Generic;
using System.Linq;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinData.Performance;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Performance
{
    internal class PointsDriversDataAdapter : PointsDataAdapterBase
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly List<TableHeader> _headers = new List<TableHeader>()
        {
            new TableHeader("Mario", Program.R.GetBitmap("Drivers.Mario.png")),
            new TableHeader("Luigi", Program.R.GetBitmap("Drivers.Luigi.png")),
            new TableHeader("Peach", Program.R.GetBitmap("Drivers.Peach.png")),
            new TableHeader("Daisy", Program.R.GetBitmap("Drivers.Daisy.png")),
            new TableHeader("Yoshi", Program.R.GetBitmap("Drivers.Yoshi.png")),
            new TableHeader("Toad", Program.R.GetBitmap("Drivers.Toad.png")),
            new TableHeader("Toadette", Program.R.GetBitmap("Drivers.Toadette.png")),
            new TableHeader("Koopa", Program.R.GetBitmap("Drivers.Koopa.png")),
            new TableHeader("Bowser", Program.R.GetBitmap("Drivers.Bowser.png")),
            new TableHeader("Donkey Kong", Program.R.GetBitmap("Drivers.DonkeyKong.png")),
            new TableHeader("Wario", Program.R.GetBitmap("Drivers.Wario.png")),
            new TableHeader("Waluigi", Program.R.GetBitmap("Drivers.Waluigi.png")),
            new TableHeader("Rosalina", Program.R.GetBitmap("Drivers.Rosalina.png")),
            new TableHeader("Metal Mario", Program.R.GetBitmap("Drivers.MetalMario.png")),
            new TableHeader("Pink Gold Peach", Program.R.GetBitmap("Drivers.PinkGoldPeach.png")),
            new TableHeader("Lakitu", Program.R.GetBitmap("Drivers.Lakitu.png")),
            new TableHeader("Shy Guy", Program.R.GetBitmap("Drivers.ShyGuy.png")),
            new TableHeader("Baby Mario", Program.R.GetBitmap("Drivers.BabyMario.png")),
            new TableHeader("Baby Luigi", Program.R.GetBitmap("Drivers.BabyLuigi.png")),
            new TableHeader("Baby Peach", Program.R.GetBitmap("Drivers.BabyPeach.png")),
            new TableHeader("Baby Daisy", Program.R.GetBitmap("Drivers.BabyDaisy.png")),
            new TableHeader("Baby Rosalina", Program.R.GetBitmap("Drivers.BabyRosalina.png")),
            new TableHeader("Larry", Program.R.GetBitmap("Drivers.Larry.png")),
            new TableHeader("Lemmy", Program.R.GetBitmap("Drivers.Lemmy.png")),
            new TableHeader("Wendy", Program.R.GetBitmap("Drivers.Wendy.png")),
            new TableHeader("Ludwig", Program.R.GetBitmap("Drivers.Ludwig.png")),
            new TableHeader("Iggy", Program.R.GetBitmap("Drivers.Iggy.png")),
            new TableHeader("Roy", Program.R.GetBitmap("Drivers.Roy.png")),
            new TableHeader("Morton", Program.R.GetBitmap("Drivers.Morton.png")),
            new TableHeader("Mii", Program.R.GetBitmap("Drivers.Mii.png")),
            new TableHeader("Tanooki Mario", Program.R.GetBitmap("Drivers.TanookiMario.png")),
            new TableHeader("Link", Program.R.GetBitmap("Drivers.Link.png")),
            new TableHeader("Villager Male", Program.R.GetBitmap("Drivers.VillagerMale.png")),
            new TableHeader("Isabelle", Program.R.GetBitmap("Drivers.Isabelle.png")),
            new TableHeader("Cat Peach", Program.R.GetBitmap("Drivers.CatPeach.png")),
            new TableHeader("Dry Bowser", Program.R.GetBitmap("Drivers.DryBowser.png")),
            new TableHeader("Villager Female", Program.R.GetBitmap("Drivers.VillagerFemale.png"))
        };

        private static readonly List<TableHeader> _headersAddMK8D = new List<TableHeader>()
        {
            new TableHeader("Gold Mario", Program.R.GetBitmap("Drivers.GoldMario.png")),
            new TableHeader("Dry Bones", Program.R.GetBitmap("Drivers.DryBones.png")),
            new TableHeader("Bowser Jr.", Program.R.GetBitmap("Drivers.BowserJr.png")),
            new TableHeader("King Boo", Program.R.GetBitmap("Drivers.KingBoo.png")),
            new TableHeader("Inkling Girl", Program.R.GetBitmap("Drivers.InklingFemale.png")),
            new TableHeader("Inkling Boy", Program.R.GetBitmap("Drivers.InklingMale.png"))
        };

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.DriverPoints);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }

        protected internal override IEnumerable<TableHeader> Rows
        {
            get
            {
                List<TableHeader> pairs = new List<TableHeader>(_headers);
                if (Program.App.BinFile.Format == BinFileFormat.MarioKart8Deluxe)
                    pairs.AddRange(_headersAddMK8D);
                return pairs.Take(Array.GetLength(0));
            }
        }

        protected internal override string RowHeaderTitle
        {
            get { return "Driver"; }
        }
    }
}
