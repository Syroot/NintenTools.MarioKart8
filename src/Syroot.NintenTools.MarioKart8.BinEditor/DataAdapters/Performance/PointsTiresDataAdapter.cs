using System.Collections.Generic;
using System.Linq;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinData.Performance;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Performance
{
    internal class PointsTiresDataAdapter : PointsDataAdapterBase
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly List<TableHeader> _headers = new List<TableHeader>()
        {
            new TableHeader("Standard", Program.R.GetBitmap("Tires.Standard.png")),
            new TableHeader("Monster", Program.R.GetBitmap("Tires.Monster.png")),
            new TableHeader("Roller", Program.R.GetBitmap("Tires.Roller.png")),
            new TableHeader("Slim", Program.R.GetBitmap("Tires.Slim.png")),
            new TableHeader("Slick", Program.R.GetBitmap("Tires.Slick.png")),
            new TableHeader("Metal", Program.R.GetBitmap("Tires.Metal.png")),
            new TableHeader("Button", Program.R.GetBitmap("Tires.Button.png")),
            new TableHeader("Off-Road", Program.R.GetBitmap("Tires.OffRoad.png")),
            new TableHeader("Sponge", Program.R.GetBitmap("Tires.Sponge.png")),
            new TableHeader("Wood", Program.R.GetBitmap("Tires.Wood.png")),
            new TableHeader("Cushion", Program.R.GetBitmap("Tires.Cushion.png")),
            new TableHeader("Blue Standard", Program.R.GetBitmap("Tires.BlueStandard.png")),
            new TableHeader("Hot Monster", Program.R.GetBitmap("Tires.HotMonster.png")),
            new TableHeader("Azure Roller", Program.R.GetBitmap("Tires.AzureRoller.png")),
            new TableHeader("Crimson Slim", Program.R.GetBitmap("Tires.CrimsonSlim.png")),
            new TableHeader("Cyber Slick", Program.R.GetBitmap("Tires.CyberSlick.png")),
            new TableHeader("Retro Off-Road", Program.R.GetBitmap("Tires.RetroOffRoad.png")),
            new TableHeader("Gold Tires", Program.R.GetBitmap("Tires.GoldTires.png")),
            new TableHeader("GLA Tires", Program.R.GetBitmap("Tires.GlaTires.png")),
            new TableHeader("Triforce Tires", Program.R.GetBitmap("Tires.TriforceTires.png")),
            new TableHeader("Leaf Tires", Program.R.GetBitmap("Tires.LeafTires.png"))
        };

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.TirePoints);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }

        protected internal override IEnumerable<TableHeader> Rows
        {
            get { return _headers.Take(Array.GetLength(0)); }
        }

        protected internal override string RowHeaderTitle
        {
            get { return "Tire"; }
        }
    }
}
