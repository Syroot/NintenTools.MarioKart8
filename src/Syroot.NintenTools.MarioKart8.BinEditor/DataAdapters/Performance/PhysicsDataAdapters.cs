using System;
using System.Collections.Generic;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinData.Performance;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Performance
{
    internal class PhysicsWeightDataAdapter : RanksDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.WeightStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }

        protected internal override IEnumerable<TableHeader> Columns
        {
            get
            {
                yield return new TableHeader("Low");
                yield return new TableHeader("High");
                yield return new TableHeader("Unknown");
            }
        }
    }

    internal class PhysicsAccelerationDataAdapter : RanksDataAdapterBase
    {
        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.AccelerationStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }

        protected internal override IEnumerable<TableHeader> Columns
        {
            get
            {
                yield return new TableHeader("Limiter");
                yield return new TableHeader("Strength");
            }
        }
    }

    internal class PhysicsOnroadDataAdapter : RanksDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.OnroadStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }

        protected internal override IEnumerable<TableHeader> Columns
        {
            get
            {
                yield return new TableHeader("Road");
            }
        }
    }

    internal abstract class PhysicsOffroadDataAdapterBase : RanksDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.OffroadStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }

        protected internal override IEnumerable<TableHeader> Columns
        {
            get
            {
                yield return new TableHeader("Dirt Light");
                yield return new TableHeader("Dirt Medium");
                yield return new TableHeader("Dirt Heavy");
                yield return new TableHeader("Sand Light");
                yield return new TableHeader("Sand Medium");
                yield return new TableHeader("Sand Heavy");
                yield return new TableHeader("Ice Light");
                yield return new TableHeader("Ice Medium");
                yield return new TableHeader("Ice Heavy");
            }
        }
    }

    internal class PhysicsOffroadSlipDataAdapter : PhysicsOffroadDataAdapterBase
    {
    }

    internal class PhysicsOffroadBrakeDataAdapter : PhysicsOffroadDataAdapterBase
    {
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected internal override Dword GetValue(int x, int y)
        {
            return Array[y][x + 9];
        }

        protected internal override void SetValue(int x, int y, Dword value)
        {
            Array[y][x + 9] = value;
        }
    }

    internal class PhysicsTurboDataAdapter : RanksDataAdapterBase
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private static readonly List<TableHeader> _headersMK8U = new List<TableHeader>()
        {
            new TableHeader("Mini-Turbo"),
            new TableHeader("Super-Turbo")
        };

        private static readonly List<TableHeader> _headersMK8D = new List<TableHeader>()
        {
            new TableHeader($"Mini-Turbo{Environment.NewLine}Frames"),
            new TableHeader($"Mini-Turbo{Environment.NewLine}Speed"),
            new TableHeader($"Mini-Turbo{Environment.NewLine}Angle"),
            new TableHeader($"Super-Turbo{Environment.NewLine}Frames"),
            new TableHeader($"Super-Turbo{Environment.NewLine}Speed"),
            new TableHeader($"Super-Turbo{Environment.NewLine}Angle"),
            new TableHeader($"Trick-Turbo{Environment.NewLine}Frames"),
            new TableHeader($"Trick-Turbo{Environment.NewLine}Speed"),
            new TableHeader($"Ultra-Turbo{Environment.NewLine}Angle"),
            new TableHeader($"Mini-Turbo{Environment.NewLine}Charge Frames"),
            new TableHeader($"Super-Turbo{Environment.NewLine}Charge Frames"),
            new TableHeader($"Ultra-Turbo{Environment.NewLine}Charge Frames"),
        };

        private static readonly bool[] _floatColumnsMK8D = new[]
        {
            false, true, true, false, true, true, false, true, true, true, true, true
        };

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.TurboStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }

        protected internal override IEnumerable<TableHeader> Columns
        {
            get { return Program.App.BinFile.Format == BinFileFormat.MarioKart8 ? _headersMK8U : _headersMK8D; }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected internal override bool IsFloatColumn(int index)
        {
            if (Program.App.BinFile.Format == BinFileFormat.MarioKart8)
                return false;
            else
                return _floatColumnsMK8D[index];
        }
    }
}
