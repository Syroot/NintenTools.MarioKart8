using System.Collections.Generic;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinData.Performance;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Performance
{
    internal abstract class HandlingDataAdapterBase : RanksDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override IEnumerable<TableHeader> Columns
        {
            get
            {
                yield return new TableHeader("Steer");
                yield return new TableHeader("Drift");
                yield return new TableHeader("Charge");
            }
        }
    }

    internal class HandlingGroundDataAdapter : HandlingDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.HandlingGroundStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }
    }

    internal class HandlingWaterDataAdapter : HandlingDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.HandlingWaterStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }
    }

    internal class HandlingAntigravityDataAdapter : HandlingDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.HandlingAntigravityStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }
    }

    internal class HandlingGlidingDataAdapter : HandlingDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.HandlingAirStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }

        protected internal override IEnumerable<TableHeader> Columns
        {
            get
            {
                yield return new TableHeader("Roll");
                yield return new TableHeader("Move");
            }
        }
    }
}
