using System.Collections.Generic;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinData.Performance;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Performance
{
    internal abstract class SpeedDataAdapterBase : RanksDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override IEnumerable<TableHeader> Columns
        {
            get
            {
                yield return new TableHeader("No Coins");
                yield return new TableHeader("10 Coins");
            }
        }
    }

    internal class SpeedGroundDataAdapter : SpeedDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.SpeedGroundStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }
    }

    internal class SpeedWaterDataAdapter : SpeedDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.SpeedWaterStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }
    }

    internal class SpeedAntigravityDataAdapter : SpeedDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.SpeedAntigravityStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }
    }

    internal class SpeedGlidingDataAdapter : SpeedDataAdapterBase
    {
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.SpeedAirStats);
                return ((DwordSectionData)section.Data).Data[0];
            }
        }

        protected internal override IEnumerable<TableHeader> Columns
        {
            get
            {
                yield return new TableHeader("Any Coins");
            }
        }
    }
}
