using System.Collections.Generic;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinData.Item;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Item
{
    internal class RaceDataAdapter : ItemDataAdapterBase
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private bool _isAISet;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal RaceDataAdapter(SectionIdentifier sectionIdentifier, RaceItemSet set, bool isAISet)
            : base(sectionIdentifier, (int)set)
        {
            _isAISet = isAISet;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        protected internal override IEnumerable<TableHeader> Rows
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.RaceDistances);
                Dword[][] distances = ((DwordSectionData)section.Data).Data[_isAISet ? 1 : 0];
                foreach (Dword[] distance in distances)
                {
                    yield return new TableHeader(distance[0].Int32.ToString());
                }
            }
        }
    }
}
