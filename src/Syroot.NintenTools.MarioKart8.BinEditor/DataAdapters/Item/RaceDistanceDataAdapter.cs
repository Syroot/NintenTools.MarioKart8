using System.Collections.Generic;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinData.Item;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters.Item
{
    internal class RaceDistanceDataAdapter : DataAdapter
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private int _raceDistanceType;
        
        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        protected internal override IEnumerable<TableHeader> Columns
        {
            get
            {
                yield return new TableHeader("Human Player");
                yield return new TableHeader("AI Player");
            }
        }

        protected internal override IEnumerable<TableHeader> Rows
        {
            get
            {
                for (int i = 0; i < Array.GetLength(0); i++)
                {
                    yield return new TableHeader(i.ToString());
                }
            }
        }

        protected internal override string RowHeaderTitle
        {
            get { return "Index"; }
        }

        protected internal override Dword[][] Array
        {
            get
            {
                Section section = Program.App.BinFile.GetSectionByID((uint)SectionIdentifier.RaceDistances);
                return ((DwordSectionData)section.Data).Data[_raceDistanceType];
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected internal override Dword GetValue(int x, int y)
        {
            _raceDistanceType = x;
            return Array[y][0];
        }

        protected internal override void SetValue(int x, int y, Dword value)
        {
            _raceDistanceType = x;
            Array[y][0] = value;
        }
    }
}
