using System;
using System.Reflection;
using System.Windows.Forms;

namespace Syroot.NintenTools.MarioKart8.BinEditor.UI
{
    /// <summary>
    /// Represents a context menu offering typical calculation methods for a <see cref="BinDataGrid"/>.
    /// </summary>
    internal class CalculationContextMenu : ContextMenuStrip
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="CalculationContextMenu"/> class.
        /// </summary>
        internal CalculationContextMenu()
        {
            ResourceLoader loader = new ResourceLoader(Assembly.GetExecutingAssembly(),
                "Syroot.NintenTools.MarioKart8.BinEditor.Resources");
            Items.AddRange(new ToolStripItem[]
            {
                new ToolStripMenuItem("Set...", loader.GetBitmap("Menu.Set.png"), Set_Clicked),
                new ToolStripSeparator(),
                new ToolStripMenuItem("Add...", loader.GetBitmap("Menu.Add.png"), Add_Clicked),
                new ToolStripMenuItem("Subtract...", loader.GetBitmap("Menu.Subtract.png"), Subtract_Clicked),
                new ToolStripMenuItem("Multiply...", loader.GetBitmap("Menu.Multiply.png"), Multiply_Clicked),
                new ToolStripMenuItem("Divide...", loader.GetBitmap("Menu.Divide.png"), Divide_Clicked),
            });
            Renderer = new VisualStudioRenderer();
        }
        
        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void Set_Clicked(object sender, EventArgs e)
        {
            BinDataGrid binDataGrid = SourceControl as BinDataGrid;
            float? value = FormCalculation.Show("Set");
            if (value.HasValue)
            {
                foreach (DataGridViewCell cell in binDataGrid.SelectedCells)
                {
                    if (binDataGrid.DataAdapter.IsFloatColumn(cell.ColumnIndex))
                    {
                        cell.Value = (float)value;
                    }
                    else
                    {
                        cell.Value = (int)value;
                    }
                }
            }
        }

        private void Add_Clicked(object sender, EventArgs e)
        {
            BinDataGrid binDataGrid = SourceControl as BinDataGrid;
            float? value = FormCalculation.Show("Add");
            if (value.HasValue)
            {
                foreach (DataGridViewCell cell in binDataGrid.SelectedCells)
                {
                    if (binDataGrid.DataAdapter.IsFloatColumn(cell.ColumnIndex))
                    {
                        cell.Value = (float)cell.Value + value;
                    }
                    else
                    {
                        cell.Value = (int)((int)cell.Value + value);
                    }
                }
            }
        }

        private void Subtract_Clicked(object sender, EventArgs e)
        {
            BinDataGrid binDataGrid = SourceControl as BinDataGrid;
            float? value = FormCalculation.Show("Subtract");
            if (value.HasValue)
            {
                foreach (DataGridViewCell cell in binDataGrid.SelectedCells)
                {
                    if (binDataGrid.DataAdapter.IsFloatColumn(cell.ColumnIndex))
                    {
                        cell.Value = (float)cell.Value - value;
                    }
                    else
                    {
                        cell.Value = (int)((int)cell.Value - value);
                    }
                }
            }
        }

        private void Multiply_Clicked(object sender, EventArgs e)
        {
            BinDataGrid binDataGrid = SourceControl as BinDataGrid;
            float? value = FormCalculation.Show("Multiply");
            if (value.HasValue)
            {
                foreach (DataGridViewCell cell in binDataGrid.SelectedCells)
                {
                    if (binDataGrid.DataAdapter.IsFloatColumn(cell.ColumnIndex))
                    {
                        cell.Value = (float)cell.Value * value;
                    }
                    else
                    {
                        cell.Value = (int)((int)cell.Value * value);
                    }
                }
            }
        }

        private void Divide_Clicked(object sender, EventArgs e)
        {
            BinDataGrid binDataGrid = SourceControl as BinDataGrid;
            float? value = FormCalculation.Show("Divide");
            if (value.HasValue && value != 0)
            {
                foreach (DataGridViewCell cell in binDataGrid.SelectedCells)
                {
                    if (binDataGrid.DataAdapter.IsFloatColumn(cell.ColumnIndex))
                    {
                        cell.Value = (float)cell.Value / value;
                    }
                    else
                    {
                        cell.Value = (int)((int)cell.Value / value);
                    }
                }
            }
        }
    }
}
