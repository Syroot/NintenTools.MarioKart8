﻿using System.Diagnostics;

namespace Syroot.NintenTools.MarioKart8.BinEditor.UI
{
    /// <summary>
    /// Represents a node in a <see cref="RowTree"/>.
    /// </summary>
    [DebuggerDisplay("RowTreeNode  {Text}, Count = {Children.Count}")]
    public class RowTreeNode
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="RowTreeNode"/> class.
        /// </summary>
        public RowTreeNode()
        {
            Children = new RowTreeNodeChildren(this);
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="RowTreeNode"/> class with the given <paramref name="text"/>
        /// to display.
        /// </summary>
        /// <param name="text">The text to display.</param>
        public RowTreeNode(string text)
        {
            Text = text;
            Children = new RowTreeNodeChildren(this);
        }

        /// <summary>
        /// Initializse a new instance of the <see cref="RowTreeNode"/> class with the given <paramref name="text"/>
        /// to display and the specified <paramref name="children"/>.
        /// </summary>
        /// <param name="text">The text to display.</param>
        /// <param name="children">The children of the node.</param>
        public RowTreeNode(string text, params RowTreeNode[] children)
        {
            Text = text;
            Children = new RowTreeNodeChildren(this);
            Children.AddRange(children);
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets or sets the text to display.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the node can be clicked.
        /// </summary>
        public bool Enabled { get; set; } = true;

        /// <summary>
        /// Gets or sets a custom object attached to this node.
        /// </summary>
        public object Tag
        {
            get; set;
        }

        /// <summary>
        /// Gets the parent <see cref="RowTreeNode"/>.
        /// </summary>
        public RowTreeNode Parent { get; internal set; }
        
        /// <summary>
        /// Gets the children of the node.
        /// </summary>
        public RowTreeNodeChildren Children { get; }

        /// <summary>
        /// Gets a value indicating whether this node has children.
        /// </summary>
        public bool IsLeaf
        {
            get { return Children.Count == 0; }
        }

    }
}
