using System;
using System.ComponentModel;
using System.Globalization;
using System.Windows.Forms;

namespace Syroot.NintenTools.MarioKart8.BinEditor.UI
{
    /// <summary>
    /// Represents a small input dialog to enter a calculation number.
    /// </summary>
    internal partial class FormCalculation : Form
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private static float _lastValue = 1;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="FormCalculation"/> class.
        /// </summary>
        internal FormCalculation()
        {
            InitializeComponent();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets or sets the value entered.
        /// </summary>
        internal float Value
        {
            get { return Single.Parse(_tbValue.Text); }
            set { _tbValue.Text = value.ToString(); }
        }

        // ---- METHODS (INTERNAL) -------------------------------------------------------------------------------------

        /// <summary>
        /// Shows the input dialog with the given title and value settings.
        /// </summary>
        /// <param name="caption">The text in the title bar of the input box.</param>
        /// <returns>The value the user entered or <c>null</c> if he canceled.</returns>
        internal static float? Show(string caption)
        {
            FormCalculation form = new FormCalculation();
            form.Text = caption;
            form.Value = _lastValue;
            
            // Show the dialog and return the value.
            if (form.ShowDialog() == DialogResult.OK)
            {
                _lastValue = form.Value;
                return form.Value;
            }
            return null;
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private void _tbValue_KeyPress(object sender, KeyPressEventArgs e)
        {
            NumberFormatInfo numberFormat = CultureInfo.CurrentCulture.NumberFormat;
            string keyString = e.KeyChar.ToString();

            bool valid = (Char.IsControl(e.KeyChar)
                || Char.IsDigit(e.KeyChar)
                || keyString == numberFormat.NegativeSign
                || keyString == numberFormat.NumberDecimalSeparator);
            e.Handled = !valid;
        }
        
        private void _tbValue_TextChanged(object sender, EventArgs e)
        {
            _btOK.Enabled = Single.TryParse(_tbValue.Text, out float f);
        }

        private void _tbValue_Validating(object sender, CancelEventArgs e)
        {
            e.Cancel = !Single.TryParse(_tbValue.Text, out float result);
        }
    }
}
