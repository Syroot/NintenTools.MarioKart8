using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace Syroot.NintenTools.MarioKart8.BinEditor.UI
{
    /// <summary>
    /// Represents a <see cref="DataGridViewRowHeaderCell"/> which can display an image together with text.
    /// </summary>
    internal class ImageDataGridRowHeader : DataGridViewRowHeaderCell
    {
        // ---- CONSTANTS ----------------------------------------------------------------------------------------------

        private const int _width = 110;

        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private string _text;
        private Image _image;
        private SizeF _scalingFactor;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageDataGridRowHeader"/> displaying the given
        /// <paramref name="text"/> and <paramref name="image"/>.
        /// </summary>
        /// <param name="text">The text to display.</param>
        /// <param name="image">The <see cref="Image"/> to display, or <c>null</c> to only display text.</param>
        /// <param name="scalingFactor">The DPI scaling factor to respect.</param>
        internal ImageDataGridRowHeader(string text, Image image, SizeF scalingFactor)
        {
            _text = text;
            _image = image;
            _scalingFactor = scalingFactor;
        }
        
        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override Size GetPreferredSize(Graphics graphics, DataGridViewCellStyle cellStyle, int rowIndex,
            Size constraintSize)
        {
            Size preferredSize = base.GetPreferredSize(graphics, cellStyle, rowIndex, constraintSize);
            Size textSize = TextRenderer.MeasureText(graphics, _text, cellStyle.Font);
            Size imageSize = _image == null ? Size.Empty : _image.Size.Scale(_scalingFactor);
            
            // Fix width of headers as it looks best when switching between tables.
            preferredSize.Width = (int)(_width * _scalingFactor.Width);
            //preferredSize.Width = cellStyle.Padding.Horizontal + Math.Max(textSize.Width, imageSize.Width / 2);
            preferredSize.Height = (imageSize.Height / 2) + cellStyle.Padding.Vertical
                + cellStyle.Padding.Top + textSize.Height;
            
            return preferredSize;
        }

        protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex,
            DataGridViewElementStates dataGridViewElementState, object value, object formattedValue, string errorText,
            DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle,
            DataGridViewPaintParts paintParts)
        {
            // Draw the background.
            base.Paint(graphics, clipBounds, cellBounds, rowIndex, dataGridViewElementState, value, formattedValue,
                errorText, cellStyle, advancedBorderStyle, paintParts);

            // Draw the image.
            Rectangle imageBounds;
            if (_image == null)
            {
                imageBounds = new Rectangle(
                    cellBounds.X + (cellBounds.Width / 2), cellBounds.Y + cellStyle.Padding.Top, 0, 0);
            }
            else
            {
                Size imageSize = _image.Size.Scale(_scalingFactor);
                imageBounds = new Rectangle(
                    cellBounds.X + (cellBounds.Width / 2) - (imageSize.Width / 4),
                    cellBounds.Y + cellStyle.Padding.Top,
                    imageSize.Width / 2, imageSize.Height / 2);
                graphics.DrawImageUnscaled(Program.R.GetSizedBitmap((Bitmap)_image, imageBounds.Size), imageBounds);
            }

            // Draw the text.
            Rectangle textBounds = new Rectangle(
                cellBounds.X, imageBounds.Bottom,
                cellBounds.Width, cellBounds.Bottom - imageBounds.Bottom);
            TextRenderer.DrawText(graphics, _text, cellStyle.Font, textBounds, cellStyle.ForeColor, cellStyle.BackColor,
                TextFormatFlags.HorizontalCenter | TextFormatFlags.VerticalCenter);
        }
    }
}
