using System;
using System.Drawing;
using System.Windows.Forms;

namespace Syroot.NintenTools.MarioKart8.BinEditor.UI
{
    /// <summary>
    /// Represents a <see cref="DataGridViewColumnHeaderCell"/> which can display an image together with text.
    /// </summary>
    internal class ImageDataGridColumnHeader : DataGridViewColumnHeaderCell
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private string _text;
        private Image _image;
        private SizeF _scalingFactor;
        private StringFormat _stringFormat;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="ImageDataGridColumnHeader"/> displaying the given
        /// <paramref name="text"/> and <paramref name="image"/>.
        /// </summary>
        /// <param name="text">The text to display.</param>
        /// <param name="image">The <see cref="Image"/> to display, or <c>null</c> to only display text.</param>
        /// <param name="scalingFactor">The DPI scaling factor to respect.</param>
        internal ImageDataGridColumnHeader(string text, Image image, SizeF scalingFactor)
        {
            _text = text;
            _image = image;
            _scalingFactor = scalingFactor;
            _stringFormat = new StringFormat
            {
                Alignment = StringAlignment.Center,
                LineAlignment = StringAlignment.Center
            };
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        protected override Size GetPreferredSize(Graphics graphics, DataGridViewCellStyle cellStyle, int rowIndex,
            Size constraintSize)
        {
            Size preferredSize = base.GetPreferredSize(graphics, cellStyle, rowIndex, constraintSize);
            Size textSize = TextRenderer.MeasureText(graphics, _text, cellStyle.Font);
            Size imageSize = _image == null ? Size.Empty : _image.Size.Scale(_scalingFactor);
            
            preferredSize.Width = cellStyle.Padding.Horizontal + Math.Max(textSize.Width, imageSize.Width / 2);
            preferredSize.Height = cellStyle.Padding.Vertical + textSize.Height;
            if (_image != null)
            {
                preferredSize.Height += imageSize.Height / 4 + cellStyle.Padding.Top;
            }

            return preferredSize;
        }

        protected override void Paint(Graphics graphics, Rectangle clipBounds, Rectangle cellBounds, int rowIndex,
            DataGridViewElementStates dataGridViewElementState, object value, object formattedValue, string errorText,
            DataGridViewCellStyle cellStyle, DataGridViewAdvancedBorderStyle advancedBorderStyle,
            DataGridViewPaintParts paintParts)
        {
            // Draw the background.
            base.Paint(graphics, clipBounds, cellBounds, rowIndex, dataGridViewElementState, value, formattedValue,
                errorText, cellStyle, advancedBorderStyle, paintParts);

            // Draw the image.
            Rectangle imageBounds;
            if (_image == null)
            {
                imageBounds = new Rectangle(cellBounds.X + (cellBounds.Width / 2), cellBounds.Y, 0, 0);
            }
            else
            {
                Size imageSize = _image.Size.Scale(_scalingFactor);
                imageBounds = new Rectangle(
                    cellBounds.X + (cellBounds.Width / 2) - (imageSize.Width / 8),
                    cellBounds.Y + cellStyle.Padding.Top,
                    imageSize.Width / 4, imageSize.Height / 4);
                graphics.DrawImageUnscaled(Program.R.GetSizedBitmap((Bitmap)_image, imageBounds.Size), imageBounds);
            }

            // Draw the text with GDI+ as TextRenderer would ignore the clipBounds.
            Rectangle textBounds = new Rectangle(
                cellBounds.X, imageBounds.Bottom,
                cellBounds.Width, cellBounds.Bottom - imageBounds.Bottom);
            graphics.Clip = new Region(clipBounds);
            using (SolidBrush br = new SolidBrush(cellStyle.ForeColor))
            {
                graphics.DrawString(_text, cellStyle.Font, br, textBounds, _stringFormat);
            }
            graphics.ResetClip();
        }
    }
}
