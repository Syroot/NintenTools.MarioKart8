using System.Drawing;

namespace Syroot.NintenTools.MarioKart8.BinEditor.UI
{
    /// <summary>
    /// Represents a pair holding text and a visualizing image.
    /// </summary>
    internal struct TableHeader
    {
        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="TableHeader"/> structure.
        /// </summary>
        /// <param name="text">The text to hold.</param>
        /// <param name="image">The <see cref="Image"/> to hold.</param>
        internal TableHeader(string text, Image image = null)
        {
            Text = text;
            Image = image;
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        /// <summary>
        /// Gets the text of the pair.
        /// </summary>
        internal string Text { get; }

        /// <summary>
        /// Gets the <see cref="Image"/> of the pair.
        /// </summary>
        internal Image Image { get; }
    }
}
