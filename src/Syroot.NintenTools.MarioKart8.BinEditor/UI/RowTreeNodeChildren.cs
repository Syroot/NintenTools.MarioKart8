﻿#pragma warning disable 1591 // Ignore well-known IList summaries for now.

using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace Syroot.NintenTools.MarioKart8.BinEditor.UI
{
    /// <summary>
    /// Represents the collection of children of a <see cref="RowTreeNode"/>.
    /// </summary>
    [DebuggerDisplay("RowTreeNodeChildren  Count = {_children.Count}")]
    public class RowTreeNodeChildren : IList<RowTreeNode>
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private RowTreeNode _parent;
        private List<RowTreeNode> _children;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        internal RowTreeNodeChildren(RowTreeNode parent)
        {
            _parent = parent;
            _children = new List<RowTreeNode>();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------

        public RowTreeNode this[int index]
        {
            get { return _children[index]; }
            set
            {
                if (value == null)
                    throw new ArgumentNullException(nameof(value));
                if (_children[index] == value)
                    return;
                if (_children.Contains(value))
                    throw new ArgumentException("The node is already a child.", nameof(value));

                _children[index].Parent = null;
                _children[index] = value;
                value.Parent = _parent;
            }
        }

        public int Count
        {
            get { return _children.Count; }
        }

        public bool IsReadOnly
        {
            get { return false; }
        }

        // ---- METHODS (PUBLIC) ---------------------------------------------------------------------------------------

        public void Add(RowTreeNode item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(ValueType));
            if (_children.Contains(item))
                throw new ArgumentException("The node is already a child.", nameof(item));

            _children.Add(item);
            item.Parent = _parent;
        }

        public void AddRange(IEnumerable<RowTreeNode> collection)
        {
            foreach (RowTreeNode node in collection)
            {
                Add(node);
            }
        }

        public void Clear()
        {
            foreach (RowTreeNode child in _children)
            {
                child.Parent = null;
            }
            _children.Clear();
        }

        public bool Contains(RowTreeNode item)
        {
            return _children.Contains(item);
        }

        public void CopyTo(RowTreeNode[] array, int arrayIndex)
        {
            _children.CopyTo(array, arrayIndex);
        }

        public IEnumerator<RowTreeNode> GetEnumerator()
        {
            return _children.GetEnumerator();
        }

        public int IndexOf(RowTreeNode item)
        {
            return _children.IndexOf(item);
        }

        public void Insert(int index, RowTreeNode item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));
            if (_children.Contains(item))
                throw new ArgumentException("The node is already a child.", nameof(item));

            _children.Insert(index, item);
            item.Parent = _parent;
        }

        public bool Remove(RowTreeNode item)
        {
            if (item == null)
                throw new ArgumentNullException(nameof(item));

            foreach (RowTreeNode node in _children)
            {
                if (node == item)
                {
                    _children.Remove(node);
                    node.Parent = null;
                    return true;
                }
            }
            return false;
        }

        public void RemoveAt(int index)
        {
            RowTreeNode node = _children[index];
            _children.RemoveAt(index);
            node.Parent = null;
        }

        // ---- METHODS ------------------------------------------------------------------------------------------------
        
        IEnumerator IEnumerable.GetEnumerator()
        {
            return _children.GetEnumerator();
        }
    }
}

#pragma warning restore 1591
