﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Syroot.NintenTools.MarioKart8.BinData;
using Syroot.NintenTools.MarioKart8.BinEditor.DataAdapters;
using Syroot.NintenTools.MarioKart8.BinEditor.Editors;
using Syroot.NintenTools.MarioKart8.BinEditor.UI;

namespace Syroot.NintenTools.MarioKart8.BinEditor
{
    /// <summary>
    /// Represents the main application.
    /// </summary>
    internal class App : Form
    {
        // ---- FIELDS -------------------------------------------------------------------------------------------------

        private EditorBase[] _editors;
        private EditorBase _currentEditor;
        private string _fileName;

        private RowTree _rowTree;
        private RowTreeNode _fileNode;

        private TableLayoutPanel _tlpFile;
        private FlatButton _fbOpen;
        private FlatButton _fbSave;
        private FlatButton _fbSaveAs;

        private BinDataGrid _binDataGrid;

        // ---- CONSTRUCTORS & DESTRUCTOR ------------------------------------------------------------------------------

        /// <summary>
        /// Initializes a new instance of the <see cref="App"/> class.
        /// </summary>
        internal App(params EditorBase[] editors)
        {
            _editors = editors;

            SuspendLayout();

            // Set up the main form.
            AllowDrop = true;
            ClientSize = new Size(1000, 600);
            DoubleBuffered = true;
            Font = SystemFonts.MessageBoxFont;
            Icon = Program.R.GetIcon("Icon.ico");
            StartPosition = FormStartPosition.CenterScreen;

            // Set up the row tree.
            _rowTree = new RowTree();
            _rowTree.Dock = DockStyle.Top;
            _rowTree.Visible = false;
            _rowTree.SelectedNodeChanging += _rowTree_SelectedNodeChanging;
            _fileNode = new RowTreeNode("File");
            Controls.Add(_rowTree);

            // Set up the file section.
            _tlpFile = new TableLayoutPanel();
            _tlpFile.Margin = new Padding(0);
            _tlpFile.Dock = DockStyle.Fill;
            for (int i = 0; i < 3; i++)
            {
                _tlpFile.RowStyles.Add(new RowStyle(SizeType.Percent, 100));
            }
            _fbOpen = new FlatButton("Open", _fbOpen_Click);
            _fbSave = new FlatButton("Save", _fbSave_Click);
            _fbSaveAs = new FlatButton("Save As", _fbSaveAs_Click);
            _tlpFile.Controls.Add(_fbOpen);
            _tlpFile.Controls.Add(_fbSave);
            _tlpFile.Controls.Add(_fbSaveAs);
            Controls.Add(_tlpFile);

            // Set up the data grid.
            _binDataGrid = new BinDataGrid();
            _binDataGrid.MinimumColumnWidth = 95;
            Controls.Add(_binDataGrid);

            CurrentEditor = null;
            UpdateUI();

            AutoScaleDimensions = new SizeF(96, 96);
            AutoScaleMode = AutoScaleMode.Dpi;
            ResumeLayout();
        }

        // ---- PROPERTIES ---------------------------------------------------------------------------------------------
        
        /// <summary>
        /// Gets the <see cref="BinData.BinFile"/> instance representing the game data.
        /// </summary>
        internal BinFile BinFile { get; private set; }

        private EditorBase CurrentEditor
        {
            get { return _currentEditor; }
            set
            {
                _currentEditor = value;

                // Clear all nodes in the navigational tree.
                _recentNodes = new Dictionary<RowTreeNode, RowTreeNode>();
                _rowTree.RootNode.Children.Clear();

                if (_currentEditor == null)
                {
                    // Hide the row tree and select the file tab.
                    _rowTree.Visible = false;
                    _rowTree.RootNode.Children.Add(_fileNode);
                    _rowTree.SelectedNode = _fileNode;
                }
                else
                {
                    // Show the tree and select the first tab.
                    _rowTree.Visible = true;
                    _rowTree.BackColorSelected = _currentEditor.AccentColor;
                    _rowTree.RootNode.Children.AddRange(_currentEditor.GetCategoryTree().Children);
                    _rowTree.RootNode.Children.Insert(0, _fileNode);
                    _rowTree.SelectedNode = _rowTree.RootNode.Children[1];
                }
                _rowTree.AdjustSize();
            }
        }

        // ---- METHODS (PROTECTED) ------------------------------------------------------------------------------------

        /// <summary>
        /// Raised when the handle was created.
        /// </summary>
        /// <param name="e">The <see cref="EventArgs"/>.</param>
        protected override void OnHandleCreated(EventArgs e)
        {
            base.OnHandleCreated(e);
            // Open a file passed as command line argument.
            string[] args = Environment.GetCommandLineArgs();
            string file = args.Length == 2 ? args[1] : null;
            if (!String.IsNullOrEmpty(file))
            {
                OpenFile(file);
            }
        }

        /// <summary>
        /// Raised when a drag &amp; drop operation enters this window's bounds.
        /// </summary>
        /// <param name="e">The <see cref="DragEventArgs"/>.</param>
        protected override void OnDragEnter(DragEventArgs e)
        {
            base.OnDragEnter(e);
            if (e.Data.GetDataPresent(DataFormats.FileDrop))
            {
                e.Effect = DragDropEffects.Move;
            }
        }

        /// <summary>
        /// Raised when a drag &amp; drop operation ends inside this window's bounds.
        /// </summary>
        /// <param name="e">The <see cref="DragEventArgs"/>.</param>
        protected override void OnDragDrop(DragEventArgs e)
        {
            base.OnDragDrop(e);
            string file = ((string[])e.Data.GetData(DataFormats.FileDrop))[0];
            OpenFile(file);
        }

        // ---- METHODS (PRIVATE) --------------------------------------------------------------------------------------
        
        private void UpdateUI()
        {
            SuspendLayout();

            // Update the window title - prepend file name and directory if a file is open.
            string title = $"{AssemblyInfo.Title} {Application.ProductVersion}";
            if (_fileName != null)
            {
                string fileName = Path.GetFileName(_fileName);
                string directoryName = Path.GetDirectoryName(_fileName);
                title = $"{fileName} ({directoryName}) - {title}";
            }
            Text = title;

            // Update the window contents.
            bool fileOpen = BinFile != null;
            _fbSave.Visible = fileOpen;
            _fbSaveAs.Visible = fileOpen;
            if (_rowTree.SelectedNode == _fileNode)
            {
                _binDataGrid.Visible = false;
                _tlpFile.Visible = true;
            }
            else
            {
                _tlpFile.Visible = false;
                _binDataGrid.Visible = true;
                _binDataGrid.Focus();
            }

            // Fix the retarded dock order of Windows Forms.
            _rowTree.BringToFront();
            _tlpFile.BringToFront();
            _binDataGrid.BringToFront();

            ResumeLayout();
        }
        
        private void OpenFile(string fileName)
        {
            try
            {
                _fileName = fileName;
                BinFile = new BinFile(_fileName);

                // Find an editor which can handle this BIN file.
                _currentEditor = null;
                foreach (EditorBase editor in _editors)
                {
                    if (editor.CheckBinFile(BinFile))
                    {
                        CurrentEditor = editor;
                        break;
                    }
                }
                if (_currentEditor == null)
                    throw new NotImplementedException("This BIN file is not supported.");
                
                UpdateUI();
            }
            catch (Exception ex)
            {
                _fileName = null;
                BinFile = null;
                CurrentEditor = null;
                UpdateUI();
                MessageBox.Show($"Could not open {Path.GetFileName(fileName)}:{Environment.NewLine}{ex.Message}",
                    AssemblyInfo.Title, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void SaveFile(string fileName)
        {
            try
            {
                BinFile.Save(fileName);
                _rowTree.SelectedNode = _rowTree.RootNode.Children[1];
            }
            catch (Exception ex)
            {
                MessageBox.Show($"Could not save \"{fileName}\":{Environment.NewLine}{ex.Message}", AssemblyInfo.Title,
                    MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        // ---- EVENTHANDLERS ------------------------------------------------------------------------------------------

        private Dictionary<RowTreeNode, RowTreeNode> _recentNodes;

        private void _rowTree_SelectedNodeChanging(object sender, RowTreeNodeChangingEventArgs e)
        {
            // Ensure that a leaf node (having a data adapter) is always selected.
            while (!e.NewNode.IsLeaf)
            {
                e.NewNode = e.NewNode.Children[0];
            }
            // Set the new data adapter.
            DataAdapter dataAdapter = (DataAdapter)e.NewNode.Tag;
            if (dataAdapter == null)
            {
                _binDataGrid.Visible = false;
                _tlpFile.Visible = true;
            }
            else
            {
                _binDataGrid.DataAdapter = dataAdapter;
                _binDataGrid.Visible = true;
                _tlpFile.Visible = false;
            }
        }

        private void _fbOpen_Click(object sender, EventArgs e)
        {
            using (OpenFileDialog openFileDialog = new OpenFileDialog())
            {
                openFileDialog.Title = "Open File";
                openFileDialog.InitialDirectory = Path.GetDirectoryName(_fileName);
                openFileDialog.FileName = Path.GetFileName(_fileName);
                openFileDialog.Filter = "Mario Kart 8 (Deluxe) BIN Files|*.bin|All Files|*.*";
                if (openFileDialog.ShowDialog() == DialogResult.OK)
                {
                    OpenFile(openFileDialog.FileName);
                }
            }
        }

        private void _fbSave_Click(object sender, EventArgs e)
        {
            SaveFile(_fileName);
        }

        private void _fbSaveAs_Click(object sender, EventArgs e)
        {
            using (SaveFileDialog saveFileDialog = new SaveFileDialog())
            {
                saveFileDialog.Title = "Save File";
                saveFileDialog.InitialDirectory = Path.GetDirectoryName(_fileName);
                saveFileDialog.FileName = Path.GetFileName(_fileName);
                saveFileDialog.Filter = "Mario Kart 8 (Deluxe) BIN Files|*.bin|All Files|*.*";
                if (saveFileDialog.ShowDialog() == DialogResult.OK)
                {
                    SaveFile(saveFileDialog.FileName);
                }
            }
        }
    }
}
